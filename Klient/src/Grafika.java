/*
 ============================================================================
 Name        : Grafika.java
 Author      : Christine Baierova, christine.baierova@gmail.com
 Version     : UPS - pexeso UDP 1.0
 Description : Hlavni GUI k aplikaci, okno s kartickami
 ============================================================================
 */

import java.awt.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.*;

import javax.swing.border.Border;
import javax.swing.text.*;

import java.awt.*;
import java.io.*;

import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")

public class Grafika extends JFrame
{
	public int RADY = 6;
	public int SLOUPCE = 6;

	public static String OBR_DIR = "obr/";				/* cesta k obrazkum	*/
	public static String OBR_FORMAT = ".jpg";			/* format obrazku 	*/

	public static String DEFAULT_ADRESA = "127.0.0.1";
	public static int DEFAULT_PORT = 20000;

	Klient klient;

	Karta[] poleKarty;
	JPanel infoPanel;
	JLabel infoLabel = new JLabel("Pexeso", JLabel.CENTER);
	int posledniKarta = 1;
	boolean dohralJsemKolo;
	boolean mrtva;
	int pocetOtocenychKaret;

	ImageIcon rubKarty,mrtvyObr;

	/**
	 * vraci klienta
	 * 
	 * @return
	 */
	public Klient getKlient()
	{
		return klient;
	}

	/**
	 * Vytvori karticku s obrazkem
	 * 
	 * @param cesta
	 *            k obrazku
	 * @param popisek
	 * @return typ ImageIcon
	 */
	protected ImageIcon vytvorImageIcon(String cesta)
	{
		BufferedImage img = null;
		try {
			 
			 img =  ImageIO.read(new File(cesta));
		
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (img != null) { 		/* pokud se obrazky najdou resp. cesta k nim... */
			return new ImageIcon(img);
		} else {
			System.err.println("Nemohu najit soubor ikony: " + cesta);
			return null;
		}
	}
	
	/**
	 * Hlavni konterjner GUI.
	 * 
	 * @return
	 */

	public Container hlavniContainer()
	{
		JPanel hlavniJP = new JPanel();
		hlavniJP.setLayout(new MigLayout());
		hlavniJP.add(kartyContainer(), "dock center");

		return hlavniJP;
	}

	/**
	 * Kontejner s kartami
	 * 
	 * @return
	 */
	private Container kartyContainer()
	{
		JPanel kartyPanel = new JPanel();
		kartyPanel.setLayout(new GridLayout(RADY, SLOUPCE));

		poleKarty = new Karta[RADY * SLOUPCE];

		rubKarty= vytvorImageIcon(OBR_DIR + "back" + OBR_FORMAT);
		mrtvyObr = vytvorImageIcon(OBR_DIR + "null" + OBR_FORMAT);
		for (int i = 0; i <RADY * SLOUPCE; i++) {
			poleKarty[i] = new Karta(rubKarty, new AkceKlik(), i);
			kartyPanel.add(poleKarty[i]);
		
		}
		return kartyPanel;
	}

	/**
	 * Konstruktor grafiky.
	 * 
	 * @param klient
	 *            instance s klientem = hlavní tridou.
	 */
	public Grafika(Klient klient) {
		this.setTitle("Snaha o Pexeso");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); /*program se ukonci po zavreni okna*/
		this.getContentPane().add(kartyContainer());
		this.setBounds(500, 100, 500, 500); 				/* kde se ma okno zobrazit pri spusteni */
		this.setMinimumSize(new Dimension(700, 720));

		infoPanel = new JPanel(new BorderLayout());
		infoPanel.add(infoLabel);
		add(infoPanel, BorderLayout.SOUTH);

		this.setVisible(true); /* vykresli */
		this.klient = klient;

	}

	public void nastavInfo(String info) {
		infoLabel.setText(info);

	}

	/**
	 * Otaci kartu dle cisla karty v poli
	 * @param kartaId
	 * @param poziceKarty
	 */
	public void otocKartu(String kartaId, int poziceKarty)
	{
		if (posledniKarta == -1) return;
		ImageIcon icon = vytvorImageIcon(OBR_DIR + Integer.parseInt(kartaId) + OBR_FORMAT);
		poleKarty[poziceKarty].setIcon(icon);
		poleKarty[poziceKarty].setOtocena(true);
		repaint();
	}

	/**
	 * Po odehrani hrace prekresli zase vsechny zbyvajici karty jako neotocena
	 */
	public void nastavNoveKolo()
	{
		for(int i =0; i< RADY * SLOUPCE; i++){
			
			poleKarty[i].setOtocena(false);
			
			if(poleKarty[i].isMrtva())
			{
				poleKarty[i].setIcon(mrtvyObr);
			}
			else
			{
				poleKarty[i].setIcon(rubKarty);
			}
		}
		
		
		revalidate();
		
	}
	
	/**
	 * Vymaze dvojici karet zaslanych serverem - nastavi na ne obrazek null
	 * @param prvniK
	 * @param druhaK
	 */
	public void nastavMrtvouDvojici(int prvniK, int druhaK){
		poleKarty[prvniK].setIcon(mrtvyObr);
		poleKarty[prvniK].setMrtva(true);
		poleKarty[druhaK].setIcon(mrtvyObr);
		poleKarty[druhaK].setMrtva(true);
		repaint();
	}

	/**
	 * Akce pri kliku na kartu, ostatni karty se deaktivuji dokud hrac neni opet na rade
	 * @author christine
	 *
	 */
	private class AkceKlik implements MouseListener
	{

		@Override
		public void mousePressed(MouseEvent e)
		{
			
		}

		@Override
		public void mouseClicked(MouseEvent e)
		{
		
			if (infoLabel.getText().equalsIgnoreCase("Hrajete"))
			{
				nastavInfo(" ");
				Karta karta = (Karta) e.getSource();
				posledniKarta = karta.cisloVpoli;
				pocetOtocenychKaret++;
						
				if (karta.mrtva || karta.isOtocena())
				{
					nastavInfo("Hrajete");
				}					
				else
				{
					klient.poslatKlikNaKartu(karta.cisloVpoli);
				}
			}

		}
		
		@Override
		public void mouseEntered(MouseEvent e)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}
	}

}
