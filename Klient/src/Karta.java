/*
 ============================================================================
 Name        : Karta.java
 Author      : Christine Baierova, christine.baierova@gmail.com
 Version     : UPS - pexeso UDP 1.0
 Description : Reprezentace karty v poli, nastaveni zda je otocena/smazana
 ============================================================================
 */

import java.awt.event.MouseListener;

import javax.swing.Icon;
import javax.swing.JButton;


@SuppressWarnings("serial")
public class Karta extends JButton{
	int cisloVpoli;
	boolean mrtva;
	boolean otocena;

	public Karta(Icon icon, MouseListener l, int cisloVpoli) {
		super(icon);
		addMouseListener(l);
		this.cisloVpoli = cisloVpoli;
		setMrtva(false);
		setOtocena(false);
	}
	
	public boolean isMrtva() {
		return mrtva;
	}

	public void setMrtva(boolean mrtva) {
		this.mrtva = mrtva;
	}
	
	public boolean isOtocena() {
		return otocena;
	}

	public void setOtocena(boolean otocena) {
		this.otocena = otocena;
	}
	
	

}
