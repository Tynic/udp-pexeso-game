/*
 ============================================================================
 Name        : Klient.java
 Author      : Christine Baierova, christine.baierova@gmail.com
 Version     : UPS - pexeso UDP 1.0
 Description : hlavni vlakno pro prijem datagramu, parsovani, pripojeni GUI
 ============================================================================
 */
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Scanner;
import java.util.Arrays;

import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.text.MaskFormatter;

import net.miginfocom.swing.MigLayout;


public class Klient implements Runnable
{

	private static final int MAX_ZNAKU = 9;
	private static final int ROZ = 6;			/* funkcni znak ze serveru */
	private static final int VYHRAL = 0;
	private static final int PROHRAL = 1;
	private static final int REMIZA = 1;
	Grafika grafika; // instance grafiky
	int port;
	boolean pripojeno;
	public static String host = "";
	byte[] odeslData = new byte[64];
	byte[] prijData = new byte[64];
	String adresa;
	String jmenoHry;
	String zaloznaPrijZprava;
	String zaloznaOdeslZprava;
	DatagramSocket socket;
	int idHry;
	boolean jsemHrac1;
	//String jmenoHrace;
	String predchoziZprava;
	int pocetOtocenychKaret= 0;
	int dobaNaKonecTahu = 2000;
	public JTextField adresaTF = new JTextField(); /* Textfield pro nacteni IP adresy serveru pro pripojeni*/
	public JTextField portTF = new JTextField(); 	/* Textfield pro nacteni portu pro pripojeni*/
	
	/**
	 * Pripoji Grafiku
	 */
	void pripojitGrafika(Grafika grafika)
	{
		this.grafika = grafika;
	}

	/*
	 * hlavni metoda
	 */
	public static void main(String[] args) throws Exception
	{
	
		Klient klient = new Klient(Grafika.DEFAULT_ADRESA, " ", 10000);
		Grafika grafika = new Grafika(klient);
	
		klient.pripojitGrafika(grafika);
		Thread klientVlakno = new Thread(klient);
		klientVlakno.start();

	}

	/* 
	 * konstruktor Klienta
	 */
	public Klient(String host, String adresa, int port)
	{
		this.adresa = adresa;
		Klient.host = host;
		this.port = port;
		this.pripojeno = false;
		try
		{
			this.socket = new DatagramSocket();
		}
		catch (SocketException e)
		{
			//e.printStackTrace();
		}

	}
	/*
	 * Posila na jakou zpravu klient kliknul
	 */
	public void poslatKlikNaKartu(int cisloKartyVpoli)
	{

		String zprava = "00"+getStrIdHry()+getHrac()+"o|"+cisloKartyVpoli+";";
		
		//zaloznaOdeslZprava = zprava;
		
		poslatZpravu(zprava);

	}
	
	/*
	 * Posila zadane zpravy
	 */
	public void poslatZpravu(String zprava)
	{
		byte[] buf = new byte[256];
		try {
			buf = zprava.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InetAddress address = null;

			try
			{
				address = InetAddress.getByName(this.adresa);
			}
			catch (UnknownHostException e)
			{
				//e.printStackTrace();
			}
			
			DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
			
			try
			{
				zaloznaOdeslZprava = zprava; 
				System.out.println("Posilam " + zprava);
				socket.send(packet);		
				
				char pom;
				
				if(zprava.length() >= 7)
				{
					pom = zprava.charAt(ROZ);
					//System.out.println(pom);
							
					if (pom == 'o')
					{
						socket.send(packet);	
						socket.send(packet);
						socket.send(packet);
					}
				}

			} 
			catch (IOException e)
			{
				e.printStackTrace();
			}

	}
	
	
	/*
	 * Okno pro zadání adresy a portu pro přihlášení
	 * při zavření se klient ukončí
	 */
	public int pripojDialog()
	{
		try
		{
			String local = "";
			pripojeno = false;		/* klient jeste nedostal zpravu od serveru */
			
			try
			{ 			
				local = InetAddress.getLocalHost().getHostAddress();				
			}
			catch(UnknownHostException e)
			{
				
				local = "127.0.0.1";
			}
			finally 
			{	

				local += ":" + port;
			}
			String adresa = "";
			//ziskani IP adresy a portu z uzivatelskeho vstupu
			adresa = (String) JOptionPane.showInputDialog(null, "IP: ", "Info", JOptionPane.INFORMATION_MESSAGE, null, null, local);

			if (adresa == null)
			{
				System.out.println("Klient ukončen uživatelem\n."); 
				System.exit(1);
			}
			//nastaveni promennych na zadane hodnoty
			this.port = Integer.parseInt(adresa.substring(adresa.indexOf(":") + 1));
			adresa = adresa.substring(0, adresa.indexOf(":"));
			
			this.adresa = adresa;

			poslatZpravu("000000k|");
			
			try
			{
				socket.setSoTimeout(1500);
			}
			catch (SocketException e1)
			{
				e1.printStackTrace();
			}
			
			byte prijatyData[] = new byte[64];			
			Arrays.fill( prijatyData, (byte) 0 );
			DatagramPacket prvniPacket = new DatagramPacket(prijatyData, prijatyData.length);
			try
			{
				System.out.println("ČEKÁM první paket...");
				socket.receive(prvniPacket);
			}
			catch (SocketTimeoutException e)
			{
				JOptionPane.showMessageDialog(null, "Nepřipojeno. "+ "", "Chyba", JOptionPane.ERROR_MESSAGE);
				return 1;
			} 
			catch (IOException e)
			{
				JOptionPane.showMessageDialog(null, "Nepřipojeno. "+ "", "Chyba", JOptionPane.ERROR_MESSAGE);
				return 1;
			}
			if(prvniPacket !=null)
			{
				String prijateZprava = new String(prvniPacket.getData());
				System.out.println("Přijato:" + prijateZprava);
				pripojeno = true;
							
				zpracujZpravu(prijateZprava);
				return 0;
			}
			
			return 0;
			
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null, "Nepřipojeno. "+ "", "Chyba", JOptionPane.ERROR_MESSAGE);
			return 1;
		}
	}

	@Override
	public void run()
	{

		int err = 1;		
			
		while (err == 1)
		{
			err = pripojDialog();	/* uvodni okno pro pripojeni k serveru */
		}

		byte prijatyData[] = new byte[64];

			
		while (true)
		{
			try
			{
				socket.setSoTimeout(55000);
			}
			catch (SocketException e1)
			{
				
				JOptionPane.showMessageDialog(null, "Pravděpodobně nastala chyba serveru, příliš dlouhá nečinnost."+ "", "Chyba", JOptionPane.OK_OPTION);
				System.exit(1);
				//e1.printStackTrace();
			}
			
			Arrays.fill( prijatyData, (byte) 0 );
			DatagramPacket prijatyPacket = new DatagramPacket(prijatyData, prijatyData.length);
			try
			{
				//System.out.println("ČEKÁM");
				socket.receive(prijatyPacket);
			}
			catch (SocketTimeoutException e)
			{
				System.out.println("Timeout 30s vypršel..." + e);
				if(predchoziZprava != null)
				{
					
				}
				else
				{
		
					vyprsel();
					prijatyPacket =null;
				}
			}
			catch (IOException e)
			{
				System.out.println("Chyba při příjmu zprávy");
				//e.printStackTrace();
			}
			
			if(prijatyPacket !=null)
			{
				String prijateZprava = new String(prijatyPacket.getData());
				
				if (prijateZprava.equals(zaloznaPrijZprava))
				{	
					continue;
				}
				else
				{		
					System.out.println("Přijato: " + prijateZprava);
					zaloznaPrijZprava = prijateZprava;
					zpracujZpravu(prijateZprava);
				}
			}
		}

	}
	
	/**
	 * Integer string ID hry zpracuje do retezce pro poslani datagramu 
	 */
	public String getStrIdHry(){
		String idHryStr  = String.valueOf(this.idHry);
		idHryStr = String.format("%03d", idHry);
		//System.out.println("idHry: " +idHry + " string: " +idHryStr);
		return idHryStr;
	}
	
	/**
	 * Vraci jestli jsek druhy nebo prvni hrac
	 * aby mohl byt vlozen do zpravy 
	 */
	public int getHrac()
	{
		if(jsemHrac1)
		{
			return 1;
		}
		else
		{
			return 2;
		}
	}
	
	/**
	 * Zpracovava prichozi zpravu ze serveru
	 */
	public void zpracujZpravu(String zprava)
	{
		
		if(zprava.length() >= 7)
		{
			char funkce = zprava.charAt(ROZ);
			//System.out.println("funkcni znak " + funkce);
			String blokDat = zprava.substring(8);

			if(funkce == 't'){
				//seznam her
				if(blokDat.charAt(0) == 'X')
				{
					int err = 1;
					while( err == 1)
					{
						err = ukazHryDialog(null);
					}
				}
				else
				{
					int err = 1;
					while( err == 1)
					{
						err = ukazHryDialog(zpracujHry(blokDat));
					}
				}
		}

		else if(funkce == 'u')			/* otocim karty */
		{
			pocetOtocenychKaret++;
			otocKartu(blokDat);
			//System.out.println("jo poslal sem to...");
			noveKolo();
				
		}
		else if(funkce == 's')			/* smazu karty */
		{
			String pole [] = blokDat.split(";");
			grafika.nastavMrtvouDvojici(Integer.parseInt(pole[0]),Integer.parseInt(pole[1]));
		}
		else if(funkce == 'h') 			/* hraju */
		{
			grafika.nastavInfo("Hrajete");

		}
		else if(funkce == 'v')		/* vyhral jsem */
		{
			grafika.nastavInfo("Konec hry");
			hraKonci(VYHRAL);
				
		}
		else if(funkce == 'x') 		/* prohral jsem */
		{
			grafika.nastavInfo("Konec hry");
			hraKonci(PROHRAL);
		}

		else if(funkce == 'r')		/* remiza */
		{
			grafika.nastavInfo("Konec hry");
			hraKonci(REMIZA);
		}
		else if(funkce == 'F')		/* fail pri prihlaseni do existujici hry */
		{
			JOptionPane.showMessageDialog(null, "Nepodarilo se pripojit do hry", "Info",
					JOptionPane.INFORMATION_MESSAGE);
		}
		else if(funkce == 'G')		/* potvrzeni pri pripojeni do hry ci zalozeni hry*/
		{
			grafika.nastavInfo("Čekám na druhého hráče");
				
			String[] pom = blokDat.split(";");
			String pomId2 = pom[0];			
			idHry = Integer.parseInt(pomId2);

		}	
		else if(funkce == 'd')		/* potvrzeni pri pripojeni do hry ci zalozeni hry*/
		{
			grafika.nastavInfo("Druhý hráč se nepřipojil\n");
			JOptionPane.showMessageDialog(null, "Druhý hráč se nepřipojil ", "Info",
							JOptionPane.INFORMATION_MESSAGE);
			poslatZpravu("000000k|");

		}
		}
		else			/* neznamy prikaz */
		{
			System.out.println("Chybný formát zprávy.");
			
		}
		 predchoziZprava = null;
		  
	}
	
	/**
	 * Kdyz vyprsi casovy limit necinnosti druheho hrace
	 */
	public void vyprsel(){
		
		poslatZpravu("");
		JOptionPane.showMessageDialog(null, "Vypršel časový limit 30 s", "Info",
				JOptionPane.CANCEL_OPTION);
		System.exit(1);
		
	}
	
	/**
	 * Tahnou se znova dve karty
	 */
	public void noveKolo()
	{
		if (pocetOtocenychKaret >= 2)
		{
			pocetOtocenychKaret=0;
			uspi(dobaNaKonecTahu);
			grafika.nastavNoveKolo();
			grafika.infoLabel.setText("Nejsi na řadě");
		}
	}
	
	/**
	 * Nastav label ze je hrac na rade 
	 * @param zprava
	 */
	public void nastavKdoHraje(String zprava)
	{
		if (jsemHrac1)
			grafika.nastavInfo("Hraj");
	}
	
	/**
	 * Ukaze adekvatni hlasku na konci hry 
	 */
	public void hraKonci(int moznost) 
	{
		
		if (moznost == 0)
		{
			JOptionPane.showMessageDialog(null, "Vyhral jste. "+ "", "Gratulujeme", JOptionPane.OK_OPTION);
		}
		else if (moznost == 1)
		{
			JOptionPane.showMessageDialog(null, "Prohrál jste. "+ "", "Smůla", JOptionPane.OK_OPTION);
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Je to nerozhodně. "+ "", "Remíza", JOptionPane.OK_OPTION);
		}
		poslatZpravu("000000k|");
		//System.exit(1);
	}
	
	/**
	 * Otoci kartu v poli dle zpravy ze serveru
	 * @param zprava
	 */
	public void otocKartu(String zprava)
	{
		String [] pole = zprava.split(";");
	
		grafika.otocKartu(pole[1], Integer.parseInt(pole[0]));
	}
	
	/**
	 * Zpracovava prijate volne hry ze serveru a da je do nabidky hraci
	 * @param zprava
	 * @return
	 */
	public String [] zpracujHry(String zprava)
	{
		String [] casti = zprava.split(";");
		String [] hry = new String [casti.length /2];
		int j=0;
		for(int i =0; i< hry.length ; i++)
		{
			hry[i] = casti[j]+"-id, " +  casti[j+1];
			j+=2;
		}
		return hry;
	}
	
	/**
	 * Okno s nabidkou volnych her
	 * @param hry
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public int ukazHryDialog(String[] hry)
	{
		JPanel panel = new JPanel(new MigLayout());
		
		JTextField fieldHry = new JTextField();

		JLabel text = new JLabel();
		JLabel novaHra = new JLabel("moje hra");
		novaHra = new JLabel("Vytvoř novou místnost (max. 9 znaků):");
		JComboBox listHer = null;
		int zvolil;
		if (hry == null) 	/* kdyz na serveru neni zadna volna hra */
		{
			String[] moznosti = { "Vytvoř hru"};
			text = new JLabel("Žádná hra volná");
			panel.add(text, "wrap");
			
			panel.add(novaHra, "wrap");
			
			panel.add(fieldHry, "span, grow");
			zvolil = JOptionPane.showOptionDialog(null, panel, "Nová hra",
					JOptionPane.NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
					moznosti, moznosti[0]);
		}
		else		/* kdyz server posle nejake volne hry */
		{
			String[] moznosti = { "Vytvoř hru", "Připoj se" };
			text = new JLabel("Vyberte si volnou hru:");

			listHer = new JComboBox(hry);
			listHer.setSelectedIndex(0);
			panel.add(text, "wrap");
			panel.add(listHer, "span, grow");	
			panel.add(novaHra, "wrap");
			panel.add(fieldHry, "span, grow");
			zvolil = JOptionPane.showOptionDialog(null, panel, "Nová hra",
					JOptionPane.NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
					moznosti, moznosti[0]);
		}
		
		if (zvolil == JOptionPane.CLOSED_OPTION)
		{
			
			System.exit(1); /* ukonci se cely klient pri zavreni okna */
		}
		
		jmenoHry = fieldHry.getText();							/* vytahne zadane jmeno hry hracem */
		
		if (jmenoHry.length() > MAX_ZNAKU)
		{		
			jmenoHry = jmenoHry.substring(0, MAX_ZNAKU);		/* osekne jmeno hry pokud je moc dlouhe */
		}
					
		for(int i = 0; i < jmenoHry.length(); i++)
		{
			if (Character.isLetter(jmenoHry.charAt(i)))
			{
				continue;
		    }
		    else
		    {
		      	System.out.println("Chyba řetězec obsahuje:" +jmenoHry.charAt(i));
		      	JOptionPane.showMessageDialog(null, "Neplatný znak v názvu hry"+ "", "Chyba", JOptionPane.ERROR_MESSAGE);
		       	return 1;
		    }
		}

			
		if (zvolil == 0) /* vytvor hru */
		{
			jsemHrac1 = true;
			
			if (jmenoHry.length() == 0)
			{
		      	System.out.println("Zadejte název hry.");
		      	JOptionPane.showMessageDialog(null, "Neplatný znak v názvu hry"+ "", "Chyba", JOptionPane.ERROR_MESSAGE);
		       	return 1;
			}
			
			poslatZpravu("000000z|" + jmenoHry + ";hrac;");

		} 
		else if (zvolil == 1) /* hrac se chce pripojit do hry */
		{
	
			String idHry = listHer.getSelectedItem().toString();
			jsemHrac1 = false;

			this.idHry = Integer.parseInt(idHry.split("-")[0]);

			poslatZpravu("000000p|" + idHry.split("-")[0] + ";hrac;");

		}
		return 0;
	}
	
	/**
	 * fce uspava na urcitou dobu
	 * @param doba
	 */
	public synchronized void uspi( int doba)
	{
		try {
			this.wait(doba);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}	
}


