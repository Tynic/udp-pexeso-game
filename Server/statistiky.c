/*
 ============================================================================
 Name        : statistiky.c
 Author      : Christine Baierova, christine.baierova@gmail.com
 Version     : UPS - pexeso UDP 1.0
 Description : vedeni statistik, trasovani komunikace
 ============================================================================
 */

#include "server.h"

FILE *soubor;

/* udaje pro tisk statstickych udaju */
extern char *delitko;
extern int port;
extern int posl_hra_id;
extern int posl_hrac_id;
extern int log_prijate_zpr;
extern int log_prijate_byt;
extern int log_odesl_zpr;
extern int log_odesl_byt;

/* casy */
time_t cas;
struct tm *p_st_cas;

/* promenne pro mereni casu */
time_t cas_start, cas_konec;

/*
 * Funkce slouzi pro vytvoreni noveho log souboru pro server
 */
void log_init()
{
	time_t cas;
	//struct tm *ltmcas;
	cas = time(NULL);
	p_st_cas = localtime(&cas);

	cas_start = time(NULL);
	soubor = fopen(SOUBOR, "w");

	if (soubor == NULL)
		perror("Chyba: nepodařilo se vytvořit logovací soubor.\n");

	fclose(soubor);
}

/*
 * Funkce prida do logu napr. kdyz je server ukoncen stiskem Ctrl+C
 */
void log_pridej(char *zpr)
{
	soubor = fopen(SOUBOR, "a+");

	flockfile(soubor);
		fprintf(soubor,"%s",zpr);
	funlockfile(soubor);
	fclose(soubor);
}

/*
 * Vytiskne prijatou i odeslanou zpravu do souboru i s casovym udajem
 */
void log_zapis(int prichozi, char *s_text)
{
  char cas_str[20];

  time(&cas);
  p_st_cas = localtime(&cas);
  //strftime(cas_str, 20, "%Y-%m-%d %X", p_st_cas);
  strftime(cas_str, 25, "%y-%m-%d, %H:%M:%S", p_st_cas);

  soubor = fopen(SOUBOR, "a+");

  flockfile(soubor);
  if (prichozi == PRIJ)
  {
    fprintf(soubor, "-> Přijato | %s | %s\n\n", cas_str, s_text);
    log_prijate_zpr++;
    log_prijate_byt+=strlen(s_text);

  }
  else if (prichozi == INFO)
  {
	  fprintf(soubor, "%s v čase %s\n\n", s_text, cas_str);
  }
  else
  {
    fprintf(soubor, "<- Odesláno | %s | %s\n\n", cas_str, s_text);
    log_odesl_zpr++;
    log_odesl_byt+=strlen(s_text);
  }
    funlockfile(soubor);

  fclose(soubor);
}

/*
 * Vytiskne statisticke udaje nakonec souboru
 */
void log_statistika()
{
	time(&cas);
	p_st_cas = localtime(&cas);
	int cas_celkem;

	soubor = fopen(SOUBOR, "a+");

	cas_konec = time(NULL);
	cas_celkem = (int) difftime(cas_konec,cas_start);

	fprintf(soubor,"Server byl ukončen v čase %s a běžel %d sekund.\n", ctime(&cas), cas_celkem);
	fprintf(soubor,"%s\n", delitko);
	fprintf(soubor,"STATISTIKA\n");
	fprintf(soubor,"%s\n",delitko);
	fprintf(soubor, "Celkový počet odehraných her: %d\n", posl_hra_id-1);
	fprintf(soubor, "Celkový pocet připojených klientů: %d\n", posl_hrac_id);
	fprintf(soubor, "Celkový počet odeslaných zpráv: %d\n", log_odesl_zpr);
	fprintf(soubor, "Celkový počet odeslaných byte: %d\n", log_odesl_byt);
	fprintf(soubor, "Celkový počet přijatých zpráv: %d\n", log_prijate_zpr);
	fprintf(soubor, "Celkový počet přijatých byte %d\n", log_prijate_byt);

	fclose(soubor);
}
