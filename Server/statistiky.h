/*
 ============================================================================
 Name        : statistiky.h
 Author      : Christine Baierova, christine.baierova@gmail.com
 Version     : UPS - pexeso UDP 1.0
 Description : hakno pro prijem datagramu, rozdelovani prikazu do her
 ============================================================================
 */

#ifndef STATISTIKY_H_
#define STATISTIKY_H_

void log_init();
void log_pridej(char *zpr);
void log_zapis(int prichozi, char *s_text);
void log_statistika();


#endif /* STATISTIKY_H_ */
