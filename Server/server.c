/*
 ============================================================================
 Name        : server.c
 Author      : Christine Baierova, christine.baierova@gmail.com
 Version     : UPS - pexeso UDP 1.0
 Description : hlavni komunikace, vlakno pro prijem datagramu, rozdelovani prikazu do her
 ============================================================================
 */

#include "server.h"
#include "statistiky.h"
#include "hra.h"

/* Globalni promenne */
	long port;

char zprava[DATA];
int zprava_len;

/* udaje pro tisk statistiky */
int posl_hra_id = 1;
int posl_hrac_id = 1;
int log_prijate_zpr = 0;
int log_prijate_byt = 0;
int log_odesl_zpr = 0;
int log_odesl_byt = 0;

char *delitko ="---------------------------------------------------------\n";

/* vlakna */
pthread_t vl_prijem;
pthread_t *vl_hry;	 /* vlákno pro kazdou hru */
pthread_mutex_t p_mutex_hry = PTHREAD_MUTEX_INITIALIZER;

void *vl_arg; /* argument pro predani odkazu na hru do vlakna hry*/
int vl_pom;

/* dulezite promenne pro server */
int server_socket, client_socket, n;
int server_len;
socklen_t client_len;
struct sockaddr_in server, client_address;

struct Hra *vsechny_hry;	/* struct Hra *ukazatel.... ukazatel->jmeno_hry */

/* deklarace funkci */
void prisla_zpr(hra **h);
void odesli_odpoved(hra **n, char prikaz, int hrac, int poz_prvni, int prvni, int druha);
void *prijem_zprav(void *arg);

/*
 * Fce pro inkrementaci zprav ve strukture hry
 * nasledne se zaktivuje vlakno hry
 */
void prisla_zpr(hra **n) {
	(*n)->zpr = ANO;
}

/*
 * funkce odesila herni tahy, zde probiha hlavni komunikace mezi obema klienty a serverem
 */
void odesli_odpoved(hra **n, char prikaz, int hrac, int poz_prvni, int prvni, int druha)
{

	char odesli[DATA];
	char buf[C_KARTY+2];
	bzero(odesli, DATA);
	bzero(buf, C_KARTY+2);
	int pocet = 0;					/* pocet odeslanych datagramu */
	int buf_len = 0;

	switch (prikaz)
	{

		case 'u': // dopsat pozici karty ??
			printf("Posilam at klienti ukazou karty...\n");
			strcpy(odesli, "000000u|");
			sprintf(buf, "%d", poz_prvni);
			buf_len = strlen(buf);
			buf[buf_len] = ';';

			strcat(odesli, buf);
			bzero(buf, C_KARTY+2);
			sprintf(buf, "%d", prvni);
			buf_len = strlen(buf);
			buf[buf_len] = ';';

			strcat(odesli, buf);

			break;
		case 's':
			printf("Posilam at klient smaze karty\n");
			strcpy(odesli, "000000s|");
				sprintf(buf, "%d", prvni); //zkontrolovat jak je to s jedno a dvoumistnyma cislicema
				if (prvni < 10)
					buf[C_KARTY-1] = ';';
				else
					buf[C_KARTY] = ';';
				strcat(odesli, buf);

				bzero(buf, C_KARTY+2);
				sprintf(buf, "%d", druha);
				if (druha < 10)
					buf[C_KARTY-1] = ';';
				else
					buf[C_KARTY] = ';';
				strcat(odesli, buf);
				break;
		case 'h':
			printf("Posilam ze hrac %d je na rade\n", hrac);
			strcpy(odesli, "000000h|");
			break;
		case 'v':
			printf("Posilam ze hrac vyhral\n");
			strcpy(odesli, "000000v|");
			break;
		case 'x':
			printf("Posilam ze hrac prohral\n");
			strcpy(odesli, "000000x|");
			break;
		case 'd':
			printf("Posilam ze timeout pro pripojeni druheho hrace vyprsel\n");
			strcpy(odesli, "000000d|");
			break;
	}

	/* dopln do zpravy cislo pro kontrolu */
	bzero(buf, C_KARTY+2);
	log_zapis(ODESL, odesli);

	/* odesle datagram, zapise do logu prvnimu hraci */
	if (hrac == PRVNI)
	{

		sendto( server_socket, &odesli, strlen(odesli), 0, (struct sockaddr*)&((*n)->client_address1), sizeof((*n)->client_address1));

		/*printf("Posilam: %s", odesli);*/

			while (pocet < POCET_ZPRAV)
			{
				sendto( server_socket, &odesli, strlen(odesli), 0, (struct sockaddr*)&((*n)->client_address1), sizeof((*n)->client_address1));
				pocet++;
			}
	}
	/* odesle datagram, zapise do logu druhemu hraci */
	else
	{
		sendto( server_socket, &odesli, strlen(odesli), 0, (struct sockaddr*)&((*n)->client_address2), sizeof((*n)->client_address2));

		/*printf("Posilam: %s", odesli);*/

		while (pocet < POCET_ZPRAV)
		{
			sendto( server_socket, &odesli, strlen(odesli), 0, (struct sockaddr*)&((*n)->client_address2), sizeof((*n)->client_address2));
			pocet++;
		}
	}
}

/*
 * Funkce vlakna pro prijem zprav od klientu
 */
void *prijem_zprav(void *arg)
{
	int i =0;
	//int init;
	char* str_h; /* hra */
	char* str_n1; /* nick 1. hrace */
	char* pom;
	char pomoc[5];
	char* str_n2;				/* nick druheho hrace ve hre */
	int id_hry, hrac;			/* do jake hry se druhy hrac chce pripojit */
	char novy_klient[300];

	hra *hra_struct;
	hra **n;
	pthread_t hra;
	char zprava_zal[DATA];

	char odesl[DATA];

	//init = malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(&p_mutex_hry, NULL);

	printf("Spouštím vlákno pro příjem zpráv...\n");

	while(TRUE)
	{
		printf("SERVER: čekám\n");
		bzero(odesl, DATA);
		bzero(pomoc, 5);
		bzero(zprava_zal, DATA);

		/* prijmi zpravu */
		zprava_len = recvfrom(server_socket, zprava_zal, sizeof(zprava_zal), 0, (struct sockaddr *) &client_address, &client_len);

		pthread_mutex_lock(&p_mutex_hry);
			strcpy(zprava, zprava_zal);
		pthread_mutex_unlock(&p_mutex_hry);

		if (zprava_len > 0)
		{
			/* data do statistiky */
			log_prijate_zpr++;
			log_prijate_byt += strlen(zprava);

			/* tiskne zpravicku */
			printf("Přijato: %s", zprava);
			//for (i = 0; i < DATA; i++) printf("%c", zprava[i]);
			printf("\n");

			/* zapis do logu prijatou zpravu */
			log_zapis(PRIJ, zprava);

			/* uzivatel ktery neni v zadne hre, ihned mu poslu bezici hry */
			if (zprava[2] == '0' &&  zprava[3] == '0' &&  zprava[4] == '0' &&  zprava[5] == '0') /* cislo spojeni je nulove, uzivatel neni v zadne hre */
			{

				if (zprava[ROZ] == 'k') /*poslu klientovi volne hry */
				{
				    sprintf(novy_klient, "Připojil se nový klient z adresy: %s a portu: %d", inet_ntoa(client_address.sin_addr), ntohs(client_address.sin_port));
					log_zapis(INFO, novy_klient); 			/* zapisu noveho klienta do logu */
					printf("Posílám uživateli právě běžící hry\n");

					if (tiskni_hry(vsechny_hry, 1) == 1) 	/* fce vytiskne bezici hry a posle je klientovi */
					{
						continue;
					}
				}
				else if (zprava[ROZ] == 'z') /* kdyz bude chtit zalozit hru, rovnou posila |NAZEV MISTNOST;NICK; */
				{

					pom = strtok(zprava, "|"); /* oznacuje pocatek dat ve zprave */
					str_h = strtok(NULL, ";"); 	/* ziskam nazev mistnosti */
					str_n1 = strtok(NULL, ";"); 	/* ziskam nick 1. hrace  */

					printf("Nick hráče: %s\n", str_n1);

					printf("SERVER: Pokouším se založit \"%s\" novou místnost s názvem: \"%s\"...\n", str_n1, str_h);

					/* zakladam novou hru */
					id_hry = posl_hra_id;

					hra_struct = pridej_hru(&vsechny_hry, str_h, str_n1, client_address);

					if (hra_struct == NULL)
					{
						printf("Error: hra nebyla vytvořena\n");
						strcpy(odesl, "000000F|");
						log_zapis(ODESL, odesl);
						sendto( server_socket, &odesl, strlen(odesl), 0, (struct sockaddr*)&(client_address), sizeof(client_address));
						continue;
					}
					else

					if (pthread_create(&hra, NULL, &prijimac_hry, (void *)hra_struct) != 0)
					{
						printf("Error: hra nebyla vytvořena\n");
						strcpy(odesl, "000000G|");
						log_zapis(ODESL, odesl);
						sendto( server_socket, &odesl, strlen(odesl), 0, (struct sockaddr*)&(client_address), sizeof(client_address));
						continue;
					}
					else
					{
						printf("Hra byla vytvořena\n");
						posl_hrac_id++;

						strcpy(odesl, "000000G|");
						sprintf(pomoc, "%d", hra_struct->id_hry);
						strcat(odesl, pomoc);
						strcat(odesl, ";");
						log_zapis(ODESL, odesl);
						sendto( server_socket, &odesl, strlen(odesl), 0, (struct sockaddr*)&(client_address), sizeof(client_address));
						continue;
					}
				}
				else if (zprava [ROZ] == 'p') /* p jako pripojit se k jiz existujici hre */
				{
					/* p|ID_mistnosti;nick2_hrac*/
					//int h2_id = posl_hrac_id;

					memcpy(pomoc, zprava+8, ID); 	/* posun o 8 pro zjisteni ID mistnosti ze zpravy */
												/* id hry bude max 3 mistne */
					//printf("pom: %s", pom);
					id_hry = atoi(pomoc);
					str_n2 = strtok(zprava, ";");
					str_n2 = strtok(NULL, ";");

					printf("SERVER: Hráč %s se chce připojit ke hře s id %d\n", str_n2, id_hry);

					/* pridavam druheho hrace k jiz existujici hre */

					n = najdi_hru(&vsechny_hry, id_hry);	/* pokousim se nalezt pozadovanou hru */

					if (n == NULL || id_hry <= 0)			/* kontrola zda hra s id existuje */
					{
						printf("Hra s id %d nenalezena, zahazuji paket\n", id_hry);

						strcpy(odesl, "000000F|");
						log_zapis(ODESL, odesl);
						sendto( server_socket, &odesl, strlen(odesl), 0, (struct sockaddr*)&(client_address), sizeof(client_address));
						continue;
					}

					/* pokousim se pripojit druheho hrace do hry */
					if (pridej_hrace(n, str_n2, client_address) == 0) /* pokud se podari zaradit hrace do hry */
					{
						strcpy(odesl, "000000G|");
						sprintf(pomoc, "%d", hra_struct->id_hry);
						strcat(odesl, pomoc);
						strcat(odesl, ";");
						log_zapis(ODESL, odesl);
						sendto( server_socket, &odesl, strlen(odesl), 0, (struct sockaddr*)&(client_address), sizeof(client_address));

						posl_hrac_id++;
					}
					else
					{
						printf("Nepodařilo se připojit hráče do hry\n");
						strcpy(odesl, "000000F|");
						log_zapis(ODESL, odesl);
						sendto( server_socket, &odesl, strlen(odesl), 0, (struct sockaddr*)&(client_address), sizeof(client_address));
						continue;
					}

					pthread_mutex_lock(&p_mutex_hry);
						prisla_zpr(najdi_hru(&vsechny_hry, id_hry)); /* upozorni vlakno hry */
					pthread_mutex_unlock(&p_mutex_hry);
				}
				else
				{
					printf("Neznámý příkaz, zahazuji paket\n");
					continue;
				}
			}
			else /* jinak je hráč zřejmě již v nějaké hře*/
			{
				pomoc[0] = zprava[2];
				pomoc[1] = zprava[3];
				pomoc[2] = zprava[4];

				id_hry = atoi(pomoc);		/* vytahnu ze zpravy jaky hrac hraje */
				hrac = zprava[ROZ-1] - '0'; 	/* prevod charu na int */

				n = najdi_hru(&vsechny_hry, id_hry);	/* pokousim se nalezt pozadovanou hru */
				if (n == NULL || id_hry <= 0)
				{
					printf("Hra s id %d nenalezena, zahazuji paket\n", id_hry);
//					strcpy(odesl, "000000F|");
					log_zapis(ODESL, odesl);
//					sendto( server_socket, &odesl, strlen(odesl), 0, (struct sockaddr*)&(client_address), sizeof(client_address));
					continue;
				}
				printf("Přišla zpráva od %d hráče ve hře s id %d.\n", hrac, id_hry);

				prisla_zpr(najdi_hru(&vsechny_hry, id_hry)); /* upozorni vlakno hry */


			}
		}
	} // konec while
	return NULL;
}

/*
 * Vypsani napovedy pro spusteni serveru
 */
void vypis_napovedu(){
	printf("\nNápověda\n");
	printf("Server spustíte příkazem:\n");
	printf("./server <adresa> <port>\n");
	printf("<adresa> může být zadána jako hostname nebo v číselném tvaru IPv4\n");
	printf("<port> může být v rozmezí 1 - 65535 \n\n");
}

/*
 * Funkce detekujici ukonceni serveru stisknutim CTRL+C
 */
void detek_handler(int sig){
	log_pridej("Ukončeno přes CTRL+C\n");
	log_statistika();
	printf("\nSERVER: Detekováno stiknutí CTRL+C. Server ukončen!\n");
	exit(1);
}



/* ---- HLAVNI ---- */

int main(int argc, char *argv[])
{
	/* zpracovani parametru */

	char prikaz[DATA]; /* pro příkazy z příkazové řádky*/

	int err;
	int i;
	i = argc;
    char ipstr[INET_ADDRSTRLEN];
	struct addrinfo hints, *res;
	char server_info[300]; /* tisk do logu */

	/* Nastaveni struktury adresy serveru*/
	memset(&server, 0, sizeof(server)); /* nulovani pameti */
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	//server.sin_port = htons(port);
	//server.sin_addr.s_addr = inet_addr(adr_buffer);

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;

	/* Vytvoreni log souboru programu*/
	log_init();

	/* Zpracuj parametry */

	/* napr: ./Pexeso "localhost" -1024 */

	if(argc >= 3) {

		/* získej port */
		port = strtol(argv[2], NULL, 10);
		err = errno;
		if (err == EINVAL) {
			fprintf(stderr, "Špatně zadán port.\n");
			exit(1);
		}

		/* pokud je port mimo rozsah*/
		if (port < 1 || port > 65535)
		{
			perror("Číslo portu je mimo rozsah 1 - 65535.\n");
			vypis_napovedu();
			exit(1);
		}

		if (port <= 1024 )
		{
			printf("Zkouším obsadit port menší než 1024, možná budou vyžadována administrátorská práva...");
		}

		err = inet_pton(server.sin_family, argv[1], (void*) &(server.sin_addr.s_addr));

		if (err != 1)
		{
			if ((err = getaddrinfo(argv[1], NULL, &hints, &res)) != 0)
			{
				perror("Špatně nastavena adresa\n");
				exit(1);
			}
			else
				/* printf("Adresa zadána jako hostname\n"); */
				server = *(struct sockaddr_in *)(res->ai_addr);
		}

		server.sin_port = htons((short) port);

		//freeaddrinfo(res);
	}
	else
	{
		vypis_napovedu();
		exit(1);
	}

	signal(SIGINT, detek_handler); /* pro detekci stisku Ctrl+c*/

	/* Vytvoreni socketu serveru */
	if ((server_socket = socket( AF_INET, SOCK_DGRAM, 0 )) < 0)
	{
		perror("Chyba: nepodařilo se vytvořit socket serveru\n");
	}


	client_len = (uint) sizeof(client_address);
    server_len = sizeof(server);

	/* svazani socketu s adresou serveru */
	if(bind(server_socket, (struct sockaddr *) &server, sizeof(struct sockaddr)) != 0){
		perror("Chyba: nepodařilo se svázat socket se jménem\n");
		exit(1);
	}

	inet_ntop(AF_INET, &(server.sin_addr), ipstr, sizeof ipstr);
	printf("Server běží na adrese: %s, port %d.\n", ipstr, ntohs(server.sin_port));

    sprintf(server_info, "Server startuje na %s a portu: %d", ipstr, ntohs(server.sin_port));
	log_zapis(INFO, server_info);

	/* zjisteni hostname */
	char host[BUFF];
	gethostname(host, BUFF-1);

	//printf("Server běží na adrese %s:%d nebo %s:%d\n", inet_ntoa(server.sin_addr), ntohs(server.sin_port), host, ntohs(server.sin_port)); /* kontrolni vypis */
	//printf("Server běží na adrese %s:%d\n", inet_ntoa(server.sin_addr), port); /* kontrolni vypis */

	/* vlakno prijmajici zpravy  */
	pthread_create(&vl_prijem, NULL, &prijem_zprav, NULL);

	/* symčka pro příjem příkazů z příkazové řádky */
	while(TRUE){
		scanf("%s",&prikaz[0]);
		if(strcasecmp(prikaz, "KONEC")==0 || strcasecmp(prikaz, "END")==0)
		{
			log_pridej("Ukončeno z příkazové řádky.\n");
			log_statistika();
			printf("\nSERVER: Ukončeno příkazem KONEC nebo END\n");
			break;
		}
		/*else if(strcmp(prikaz, "HRY")==0)
		{
			printf("\nBěžící hry:\n");
			tiskni_hry(vsechny_hry, 0);
		}*/
		else if(strcmp(prikaz, "HELP")==0)
		{
			printf("KONEC (END): ukončí server\n");
		/*	printf("HRY: vypíše právě probíhající hry\n");*/
		}
		else
		{
			printf("Neznámý příkaz, použijte HELP pro nápovědu.\n");
		}

	}

	printf("Server ukončuje svoji činnost...\n");

	for (i = 0; i < posl_hra_id; i++)
	{


	}
	exit(0);

	close(server_socket); /* zavřu socket serveru */
	return 0;
}


