/*
 ============================================================================
 Name        : server.h
 Author      : Christine Baierova, christine.baierova@gmail.com
 Version     : UPS - pexeso UDP 1.0
 Description : hlavickovy soubor s konstantami a potrebnymi knihovnami
 ============================================================================
 */

#ifndef SERVER_H_
#define SERVER_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <sys/un.h>
#include <unistd.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <malloc.h>
#include <sys/time.h>
#include <pthread.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/resource.h>
#include <netdb.h>

#define BUFF 255
#define TRUE 1
#define DATA 64				/* velikost zpravy v datagramu */
#define SOUBOR "log/soubor.txt"
#define JMENO 20			/* max kapacita pole pro jmeno hrace/nazev hry */
#define ROZ 6 				/* jako rozkaz - prvek v datagramu */
#define MAX_H 2 			/* max počet hracu v jedne hre */

#define ID 3				/* id bude max 3-ciferne */
#define KARTY 36			/* pocet karet ve hre */
#define C_KARTY 2			/* pro ziskani cisla odehrane karty ze zpravy*/
#define CEKANI 300		/* PRENASTAVIT ... jak dlouho cekame na odehrani hrace */
#define PRVNI 1				/* pro manipulaci s hraci, spise pro vyssi prehlednost */
#define DRUHY 2
#define ODESL 0
#define PRIJ 1				/* tisk do statistik - nova zprava */
#define INFO 2				/* tisk do statistik - novy klient, start serveru */
#define ANO 1
#define NE 0
#define POCET_ZPRAV 4

extern int server_socket, client_socket, n;
extern struct sockaddr_in server_address, client_address;


#endif
