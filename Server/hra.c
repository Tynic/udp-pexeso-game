/*
 ============================================================================
 Name        : hra.c
 Author      : Christine Baierova, christine.baierova@gmail.com
 Version     : UPS - pexeso UDP 1.0
 Description : zdrojovy soubor s hernimi zalezitostmi k chodu pexesa
 ============================================================================
 */

#include "server.h"
#include "hra.h"

extern pthread_mutex_t p_mutex_hry;
extern char zprava[DATA];
extern char *delitko;
extern int posl_hra_id;
extern struct Hra *vsechny_hry;

/* vlakno co se bude starat jen o prikazy ve hre */
void *prijimac_hry(void *arg)
{
	// vlakno bude prijmat POUZE OD DVOU HRACU do sve hry

	char tah[DATA];
	char odesl[DATA];
	char tahZAL[DATA];
	hra *n; 				/* pretypovani parametru */
	n = (hra *)arg;
	int ukaz, hrac, na_rade = 0, konec_hry = 0, cas = 0;
	char prikaz;
	int kontr;
	int k1, k2;					/* pomocne promenne pro indexovani karet v poli */
	int karta1, karta2; 		/* promenne pro porovnani, zda se karty shoduji */
	char pom[3];				/* pomocne pole pro vytazeni cisla karty ze zpravy */
	int id_hry = (*n).id_hry; 	/* pomocna promenna pro prehlednejsi manipulaci s id hry*/

	strcpy(tahZAL, "S");

	while(konec_hry == NE)
	{
		printf("Vlákno hry s id %d čeká...\n", id_hry);

		timeout_odehrani(&n);

		cas = 0;

		pthread_mutex_lock(&p_mutex_hry);
		bzero(tah,DATA);  					/* vycistim buffer pro zpravu */
		strcpy(tah, zprava); 				/* zkopiruji si zpravu z globalni promenne do lokalni*/
		pthread_mutex_unlock(&p_mutex_hry);

		(*n).zpr = NE; 						/* jiz zpracovavam zpravu, mohu vynulovat promennou */

		if (strcmp(tah, tahZAL) == 0) continue;
		else {
			strcpy(tahZAL, tah);
		}
		bzero(odesl,DATA);

		hrac = tah[ROZ-1] - '0'; 	/* zpravu nam posila prvni/druhy hrac ? */

		/* ---------------------------- herni zalezitosti -------------------------------*/
		if (tah[ROZ] == 'p' )
		{
			printf("Připojil se 2. hráč do hry\n");
			prikaz = 'h';

			odesli_odpoved(&n, prikaz, PRVNI, 0, 0, 0); /* posilam 1. hraci ze muze hrat */
		}
		else if (tah[ROZ] == 'o' ) 			/* hrac chce otocit kartu */
		{
			memcpy(pom, tah+8, C_KARTY); 		/* posun pro zjisteni jakou kartu odkryvame ze zpravy */
			pom[C_KARTY] = '\0';
			ukaz = atoi(pom);				/* vytahnu cislo karty ze zpravy */

			printf("Hra %d: Hráč %d si přeje odkrýt kartu č.%d\n", ((*n).id_hry), hrac, ukaz);

			if (((*n).stav == PRVNI) && (hrac == PRVNI)) /* odkryj kartu, stav = 1, na rade je prvni hrac */
			{
				na_rade++;

				if (na_rade == PRVNI) 				/* kdyz je otocena 1. karta */
				{
					karta1 = odkryj_kartu(&n, ukaz);
					k1 = ukaz;							/* ukladam index 1. karty v poli */
					ukaz_kartu(&n, k1, karta1);
					na_rade = 2;
					prikaz = 'h';
					odesli_odpoved(&n, prikaz, PRVNI, 0, 0, 0);
					continue;
				}
				else
				{
					karta2 = odkryj_kartu(&n, ukaz); 	/* pamatuju si kartu z 2. tahu*/
					(*n).stav = DRUHY; 					/* prvni hrac ma odehrano */
					k2 = ukaz;								/* ukladam index 2. karty v poli */
					na_rade = 0;
					ukaz_kartu(&n, k2, karta2);
				}

				if (karta1 == karta2) 			/* pokud se obe karty shoduji... */
				{
					((*n).h1_skore)++; 			/* zvysuji skore prvnimu hraci */
					(*n).stav = PRVNI;			/* a DRUHY hrac hraje znovu */
					(*n).herni_pole[k1] = 0;	/* vynuluju si kartu v poli, uz s ni nebude nikdo hrat */
					(*n).herni_pole[k2] = 0;
					na_rade = 0;

					smaz_karty(&n, k1, k2);

					if ( konec_kola(&n) == ANO)	/* pokud to byly posledni karty ... */
					{

						printf("Hra %d: Konec!\n", ((*n).id_hry));
					}
					else
					{
						prikaz = 'h';
						odesli_odpoved(&n, prikaz, PRVNI, 0, 0, 0);
						continue;
					}
				}
				else
				{
					prikaz = 'h';
					odesli_odpoved(&n, prikaz, DRUHY, 0, 0, 0);
					continue;
				}
			}

			if (((*n).stav == DRUHY) && (hrac == DRUHY)) /* odkryj kartu, stav = 1, na rade je prvni hrac */
			{
				na_rade++;

				if (na_rade == 1) 				/* kdyz je otocena 1. karta */
				{
					karta1 = odkryj_kartu(&n, ukaz);
					k1 = ukaz;							/* ukladam index 1. karty v poli */
					ukaz_kartu(&n, k1, karta1);
					na_rade = 1;
					prikaz = 'h';
					odesli_odpoved(&n, prikaz, DRUHY, 0, 0, 0);
					continue;
				}
				else
				{
					karta2 = odkryj_kartu(&n, ukaz); 	/* pamatuju si kartu z 2. tahu*/
					(*n).stav = PRVNI; 				/* prvni hrac ma odehrano */
					k2 = ukaz;							/* ukladam index 2. karty v poli */
					na_rade = 0;
					ukaz_kartu(&n, k2, karta2);
				}

				if (karta1 == karta2) 			/* pokud se obe karty shoduji... */
				{
					((*n).h1_skore)++; 			/* zvysuji skore prvnimu hraci */
					(*n).stav = DRUHY;			/* a DRUHY hrac hraje znovu */
					(*n).herni_pole[k1] = 0;	/* vynuluju si kartu v poli, uz s ni nebude nikdo hrat */
					(*n).herni_pole[k2] = 0;
					na_rade = 0;
					smaz_karty(&n, k1, k2);

					if ( konec_kola(&n) == ANO)	/* pokud to byly posledni karty ... */
					{
						printf("Hra %d: Konec!\n", ((*n).id_hry));
					}
					else
					{
						prikaz = 'h';
						odesli_odpoved(&n, prikaz, DRUHY, 0, 0, 0);
						continue;
					}
				}
				else
				{
					prikaz = 'h';
					odesli_odpoved(&n, prikaz, PRVNI, 0, 0, 0);
					continue;
				}
			}
		}

		else if (tah[ROZ] == 'e' ) 			/* hrac chce otocit kartu */
		{
				printf("Hrac chce ukončit hru!\n");
		}
		else
		{
			printf("Neznámý příkaz, zahazuji paket\n");
		}

	}

	return NULL;
}
/*
 * Timeout pro odehrani hrace a nasledna obsluha
 */
void timeout_odehrani(hra **n)
{
	char prikaz;
	int cas = 0;

	while((*n)->zpr == NE) /* dokud od nikoho neprisla zadna zprava... */
	{
		/* po vyprseni timeoutu odehrani/pripojeni do hry */
		if (cas > CEKANI) //30 sek
		{
			printf("Vypršela doba pro odehrání, hra s id bude %d ukončena.\n", (*n)->id_hry);

			if (((*n)->stav == PRVNI) && ((*n)->pocet==MAX_H))
			{
				prikaz = 'v';
				odesli_odpoved(n, prikaz, DRUHY, 0, 0, 0);
			}
			else if ((*n)->pocet==1)			/* do hry se nikdo nepripojil do daneho casoveho limitu */
			{
				prikaz = 'd';
				odesli_odpoved(n, prikaz, PRVNI, 0, 0, 0);
			}
			else if ((*n)->stav == DRUHY)
			{
				prikaz = 'v';
				odesli_odpoved(n, prikaz, PRVNI, 0, 0, 0);		/* poslu, ze 2. hrac vyhral */
			}

			sleep(3);
			(*n)->id_hry = -1;
			(*n)->pocet = 0;
			//odeber_hru(najdi_hru(&vsechny_hry, (*n)->id_hry));
			pthread_exit(0);
		}
		usleep(100000); //0,1s
		cas++;
	}

}
/*
 * Zjisti jestli hraci odkryly posledni dvojici karet
 */
int konec_kola(hra **n)
{
	if (((*n)->h1_skore + (*n)->h2_skore) == (KARTY/2))
	{
		kdo_vyhral(n);
		return ANO;
	}
	else
	{
		return NE;
	}
}
/*
 * Funkce zjisti, co se ukryva pod neotocenou kartou
 */
int odkryj_kartu(hra **n, int ukaz)
{
	int hodnota_karty = -1;

	if (((*n)->herni_pole[ukaz] >= 1) && ((*n)->herni_pole[ukaz] <= (KARTY/2) )) /* pokud je v poli hry opravdu neotocena karta... */
	{
		printf("Hra %d: Karta na pozici %d ma hodnotu %d\n", ((*n)->id_hry), ukaz, ((*n)->herni_pole[ukaz]));
		hodnota_karty = (*n)->herni_pole[ukaz];
		return hodnota_karty;
	}

	return hodnota_karty;

}
/*
 * Fce zjisti kdo zvitezil a odesle hracum info
 */
void kdo_vyhral(hra **n)
{
	char prikaz;

	if ((*n)->h1_skore > (*n)->h2_skore)
	{
		sleep(2);
		prikaz = 'v';
		odesli_odpoved(n, prikaz, PRVNI, 0, 0, 0);		/* poslu, ze 1. hrac vyhral */
		prikaz = 'x';
		odesli_odpoved(n, prikaz, DRUHY, 0, 0, 0);		/* poslu, ze 2. hrac prohral */
	}
	else if ((*n)->h1_skore < (*n)->h2_skore)
	{
		sleep(2);
		prikaz = 'v';
		odesli_odpoved(n, prikaz, DRUHY, 0, 0, 0);		/* poslu, ze 2. hrac vyhral */
		prikaz = 'x';
		odesli_odpoved(n, prikaz, PRVNI, 0, 0, 0);		/* poslu, ze 1. hrac prohral */

	}
	else
	{
		prikaz = 'r';
		odesli_odpoved(n, prikaz, DRUHY, 0, 0, 0);		/* poslu, ze 2. hrac vyhral */
		odesli_odpoved(n, prikaz, PRVNI, 0, 0, 0);		/* poslu, ze 1. hrac prohral */
	}

}

void ukaz_kartu(hra **n, int pozice, int karta)
{
	char prikaz = 'u'; 					/* rozkaz pro odkryti karty */

	odesli_odpoved(n, prikaz, PRVNI, pozice, karta, 0); /* posilam, at klient odkryje prvni kartu */
	odesli_odpoved(n, prikaz, DRUHY, pozice, karta, 0);

}

void smaz_karty(hra **n, int karta1, int karta2)
{
	char prikaz = 's';									/* prikaz ze budou odstraneny 2 karty */
	odesli_odpoved(n, prikaz, PRVNI, 0, karta1, karta2);  /* klient smaze shodne karty z pole */
	odesli_odpoved(n, prikaz, DRUHY, 0, karta1, karta2);
}

/*
 * odebrani hry (remove head)
 * */
void odeber_hru(hra **p)
{
	if (p && *p) {
		hra *n = *p;
		*p = (*p)->dalsi;

		free(n);
	}
}

/* list_remove(list_search(&games, 5));  => odstrani hru s ID = 5 */

/*
 * nalezeni hry podle jejiho id (game_id)
 */
hra **najdi_hru(hra **n, int id)
{
	if ((*n) != NULL)
	{
		while ((*n) != NULL)
		{
			if ((*n)->id_hry == id) return n;
			n = &(*n)->dalsi;
		}
	}
	return NULL;
}

/*
 * Fisher - Yates algoritmus na promichani karet hry
 * nageneruji si pole karet a nasledne promicham
 */
void karty(hra **n)
{
	int i, j, temp;
	j = KARTY-1;

	for (i = 0; i < (KARTY/2); i++) /* nageneruju si pole hodnot od 1 do 18 */
	{
		(*n)->herni_pole[i] = i+1;
		(*n)->herni_pole[j] = i+1;
		j--;

	}
	i = KARTY-1;
	srand(time(NULL));

	while (i > 0)  /* Fisher - Yates algoritmus na promichani pole karet */
	{
		j = rand() % (i+1);
		temp = (*n)->herni_pole[i];
		(*n)->herni_pole[i] = (*n)->herni_pole[j];
		(*n)->herni_pole[j] = temp;
		i = i - 1;
	}
	printf("Karty promíchány a připraveny.\n");
}


/*
 * pridani druheho hrace do hry a spusteni herniho vlakna
 */

int pridej_hrace(hra **n, char *h2_nick, struct sockaddr_in client_address)
{
	if ((*n)->pocet == MAX_H)
	{
		printf("Hra s id %d je již plná.\n", (*n)->id_hry);
		return 1;
	}
	(*n)->pocet = MAX_H;
	(*n)->stav = 1;
	(*n)->client_address2 = client_address;
	strcpy((*n)->h2_nick, h2_nick);
	karty(n);

	return 0;
}

/*
 * tiskne hry na konzoli a pokud prisla zadost od klienta, posle zpravy s aktualne bezicimi hrami uzivateli
 */

int tiskni_hry(hra *hlava, int poslat) {

	char odesli[DATA];
	char str[ID];

	poslat = ANO;

	if (hlava != NULL)
	{
	    bzero(odesli, DATA);
        strcpy(odesli, "000000t|"); 	/* nakopiruji ridici zahlavi zpravy*/
        int pocet_volnych = 0;
		while (hlava != NULL)
		{

			/* printf("%s", delitko);
				printf("HRA \"%s\" s id: %d \n", hlava->jmeno_hry, hlava->id_hry);*/

            if (((hlava->id_hry) != -1) && (hlava->pocet < MAX_H))
            {
                sprintf(str, "%d", hlava->id_hry);
                strcat(odesli, str);	/* do zpravy vlozim id hry, nazev, nick 1. hrace a pokud je tak i 2.*/
                strcat(odesli,";");
                strcat(odesli,hlava->jmeno_hry);
                strcat(odesli,";");
                pocet_volnych++;

            }

			hlava = hlava->dalsi;
		}
		if (poslat == ANO) /* posleme hraci zpravu s aktualnimi hrami na serveru */
        {

            if(pocet_volnych>0){

            	sendto( server_socket, &odesli, strlen(odesli), 0, (struct sockaddr*)&(client_address), sizeof(client_address));

            }
            else
            {
              strcat(odesli, "X;");
              sendto( server_socket, &odesli, strlen(odesli), 0, (struct sockaddr*)&(client_address), sizeof(client_address));
            }
            log_zapis(ODESL, odesli);
        }
	}
	else
	{
		/* printf("Není spuštěna žádná hra.\n");*/


		if (poslat == 1) /* posleme klientovi, za aktualne zadne hry nejsou spusteny */
		{
			strcpy(odesli, "000000t|X;"); 	/* nakopiruji ridici zahlavi zpravy*/
			log_zapis(ODESL, odesli);
			sendto( server_socket, &odesli, strlen(odesli), 0, (struct sockaddr*)&(client_address), sizeof(client_address));
		}
		return 1;
	}
	return 0;
}

/*
 * prida novou hru do seznamu her
 * prvni hrac je vzdy ten, co hru zaklada
 */
hra *pridej_hru(hra **hlava,  char *jmeno_hry, char *h1_nick, struct sockaddr_in client_address)
{
	hra *novahra, *p;

	novahra = (hra *)malloc(sizeof(hra)); /* alokuju pamet pro novou herni strukturu */
	novahra->id_hry = posl_hra_id; /* id hry */
	posl_hra_id++;
	novahra->stav = 0;

	strcpy(novahra->h1_nick, h1_nick); /* zkopiruju nick 1.hrace */
	strcpy(novahra->jmeno_hry, jmeno_hry); /* zkopiruju jmeno hry */

	novahra->h1_skore = 0;
	novahra->h2_skore = 0;
	novahra->client_address1 = client_address;
	novahra->zpr = 0;
	novahra->pocet = 1;

	//memset(&novahra->client_address1, 0, sizeof(client_address));

	/* zarazeni hry do seznamu her */
	if (*hlava != NULL)
	{
		p = *hlava;
		while (p->dalsi) p = p->dalsi;
		p->dalsi = novahra;
	}
	else
	{
		*hlava = novahra;
	}

	return novahra;
}
