/*
 ============================================================================
 Name        : hra.h
 Author      : Christine Baierova, christine.baierova@gmail.com
 Version     : UPS - pexeso UDP 1.0
 Description : hlavickovy soubor s hlavni herni strukturou
 ============================================================================
 */

#ifndef HRA_H_
#define HRA_H_

/* struktura drzujici si informace o kazde hre */
typedef struct Hra {
	int id_hry;
	char jmeno_hry[JMENO];

	char h1_nick[JMENO];
	int h1_skore;
	struct sockaddr_in client_address1;

	char h2_nick[JMENO];
	int h2_skore;
	struct sockaddr_in client_address2;

	int herni_pole[KARTY];
	int zpr;						/* zda do hry ceka nejaka zprava*/

	int stav;						/* kdo z hracu je na rade */
	int pocet;						/* kolik hracu je jiz ve hre */

	struct Hra *dalsi; /* odkaz na dalsi */
} hra;

extern void odesli_odpoved(hra **n, char prikaz, int hrac, int poz_prvni, int prvni, int druha);
extern void log_zapis(int prichozi, char *s_text);

void *prijimac_hry(void *arg);
hra **najdi_hru(hra **n, int id);
void odeber_hru(hra **p);
int pridej_hrace(hra **n, char *h2_nick, struct sockaddr_in client_address);
void karty(hra **p);
int tiskni_hry(hra *hlava, int poslat);
hra *pridej_hru(hra **hlava,  char *jmeno_hry, char *h1_nick, struct sockaddr_in client_address);
int konec_kola(hra **n);
void ukaz_kartu(hra **n, int pozice, int karta);
void smaz_karty(hra **n, int karta1, int karta2);
void kdo_vyhral(hra **n);
int najdi_kartu(hra **n, int ukaz);
int odkryj_kartu(hra **n, int ukaz);
void timeout_odehrani(hra **n);


#endif /* HRA_H_ */
